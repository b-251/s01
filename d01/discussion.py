print("Hello World!")

# [SECTION] Declaring Variables

age = 29
middle_initial = "D"

# Python allows assigning of values to multiple variables within a single line

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# [SECTION] Data Types
# 1. Strings
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers - for integers, decimals, and complex numbers / snakecase for choice
number_of_days = 365 # This is an integer
pi_approximation = 3.1416 # This is a decimal
complex_num = 1 + 5j # This is a complex number, j represents imaginary components

# 3. Boolean - true or false values / camelCase for choice
isLearning = True
isDifficult = False

# [SECTION] Using variables
print("My name is " + full_name)
print("My age is " + str(age)) # Typecasting parses data to convert it to a different data type

print(int(3.5)) # Converts float/decimal value into an integer/whole number
print(float(5)) # Converts integer into a float/decimal value

# F-Strings
# Another way to concatenate and it ignores the strict typing needed with regular concatenation using the '+' operator
print(f"Hi! My name is {full_name} and my age is {age}")


# [SECTION] Operations
# Arithmetic Operators - for mathematical operations
print(1 + 10)# Addition
print(15 - 8)# Subtraction
print(18 * 9)# Multiplication
print(21 / 7)# Division
print(18 % 4)# Modulo (gets the remainder of division) 18 / 4 Remainder = 2
print(2 ** 6)# Exponent

# Assignment Operators = for assigning values to variables
num1 = 3
num1 += 4
print(num1)

# Comparison Operators
print(1 == 1) # != <> <= >=

#Logical Operator
print(True and False)
print(not False)
print(False or True)