print("Hello")

name = "Jeffrey"
surname = "Bezos"
age = 59
occupation = "Project Manager"
movie = "The Internship"
rating = 88.88

print(f"I am {name} {surname}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 12
num2 = 68
num3 = 88

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)
